//
//  FirstViewController.m
//  SportsPhotos
//
//  Created by Philip Huffman on 2015-07-14.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import "FirstViewController.h"

@interface FirstViewController ()

@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.loginButton = [[FBSDKLoginButton alloc] init];
    self.cover = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"sportsphotoslogo.png"]];
    self.logoView = [[UIView alloc] initWithFrame:CGRectMake(0,0,213,56)];
}

- (void) viewWillLayoutSubviews {
    [self positionFBLoginButton];
    [self positionLogo];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) positionFBLoginButton {
    CGPoint fbLoginButtonCenter = CGPointMake(self.view.center.x,
                                              self.view.frame.size.height -
                                              self.loginButton.frame.size.height * verticalFBLoginAdjustmentFactor);
    self.loginButton.center = fbLoginButtonCenter;
    [self.view addSubview:self.loginButton];
}

- (void) positionLogo {
    CGFloat logoWidth = MIN(426.0, self.view.frame.size.width * 0.75);
    CGPoint logoCenter = CGPointMake(self.view.center.x, self.view.frame.size.height * verticalLogoAdjustmentfactor);
    self.cover.frame = CGRectMake(0.0, 0.0, logoWidth, logoWidth * 56.0 / 213.0);
    self.cover.center = logoCenter;
    [self.view addSubview:self.cover];
}

@end
