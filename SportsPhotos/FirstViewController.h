//
//  FirstViewController.h
//  SportsPhotos
//
//  Created by Philip Huffman on 2015-07-14.
//  Copyright (c) 2015 Philip Huffman. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>

CGFloat const verticalFBLoginAdjustmentFactor = 2.75;
CGFloat const verticalLogoAdjustmentfactor = 0.333;

@interface FirstViewController : UIViewController <UIViewControllerTransitioningDelegate>

@property (strong, nonatomic) UIImageView *cover;
@property (strong, nonatomic) FBSDKLoginButton *loginButton;
@property (strong, nonatomic) UIView *logoView;

@end

